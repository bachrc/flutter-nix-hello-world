# Borrowed from https://github.com/gitpod-io/template-nix/blob/23e258d392bee430e713638de148e566a90e2b00/.gitpod.Dockerfile
FROM gitpod/workspace-nix

ADD flake.nix .
ADD flake.lock .
ADD nix/ ./nix

RUN . /home/gitpod/.nix-profile/etc/profile.d/nix.sh \
  && nix develop --command echo "Dependencies downloaded"